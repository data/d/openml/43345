# OpenML dataset: Quora_Insincere_Questions_2018

https://www.openml.org/d/43345

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
It's the preprocessed train data from  Quora Insincere Questions competition 2018 The original train data is preprocessed to remove stop words, numbers, punctuations, common words and converted to lower case. The resultant data set is lemmatised and stemmed with scikit-learn/NLTK library.
Content
It contains approximately 1.3 million rows of quora questions with target =0 for sincere questions and target=1 for insincere questions.
Acknowledgements
Thanks for Co-learning lounge mentors to help me to work on this problem
Inspiration
It's very handy to build the ML models in NLP.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43345) of an [OpenML dataset](https://www.openml.org/d/43345). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43345/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43345/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43345/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

